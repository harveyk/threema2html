# threema2html.awk - Convert a Threema chat export to nicely formatted, long term preservation friendly HTML
#
# Copyright (c) 2023 Heinz Werner Kramski-Grote, kramski@web.de
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
# 
# Mini HowTo:
#
# 1. Export a chat in Threema (including media files, see https://threema.ch/en/faq/chatexport).
# 2. Unpack the .zip file (without any subfolders) into the folder where this .awk script lives.
# 3. gawk -f threema2html.awk messages.txt > index.html 
#    (See `gawk -f threema2html.awk -- -h` for more options. Use `-i` for experimental support for exports made on Apple iOS devices.)
# 4. Copy relevant media files: 
#    grep "href=\"./media/" index.html | cut -d= -f2 | cut -d\" -f2 | cut -d/ -f3 > medialist.txt 
#    xargs --arg-file=medialist.txt cp --target-directory=./media/ 
# 5. Adjust ./lib/default.css.
#
# This script has largely been tested on Android based exports only. 
# Support for exports made on Apple iOS devices is experimental and currently limited to German.
#
# https://codeberg.org/harveyk/threema2html/


@include "getopt.awk"

#------------------------------------------------------------------------------
function usage()
#------------------------------------------------------------------------------
{
    print Banner
    print "Usage: gawk -f threema2html.awk [-- options] inputfile [> outputfile]"
    print "\toptions:"
    print "\t\t-f<date>   \tStart of date range to include (default: 19700101)" 
    print "\t\t-t<date>   \tEnd of date range to include (default: 20701231)" 
    print "\t\t-T<title>  \tHTML title (default: \"Threema Export\")"
    print "\t\t-n<name>   \tName of exporter to be substituted for user \"Ich\", \"Me\" or \">>>\" (default: none)"
    print "\t\t-p<name>   \tName of communication partner to be substituted for user \"<<<\" (default: none, iOS exports only)"    
    print "\t\t-m<folder> \tMedia folder (default: \"./media\")"
    print "\t\t-s<style>  \tCSS style file (default: \"./lib/default.css\")"
    print "\t\t-w<width>  \tImage width (default: 480)"
    print "\t\t-u         \tAssume US-style date format mm/dd/yyyy or mm.dd.yyyy (default: dd/mm/yyyy or dd.mm.yyyy)"
    print "\t\t-i         \tiOS mode, expect iPhone/iPad export format"
    print "\t\t-v         \tVerbose"
    print "\t\t-h         \tHelp"
    print "\tinputfile:   \t\tExported Threema messages file (.txt)"
    print "\toutputfile:  \t\tOutput file (.html)"
}

#------------------------------------------------------------------------------
BEGIN {
#------------------------------------------------------------------------------
    
    # Defaults
    DateFrom = 19700101
    DateTo   = 20701231
    Title = "Threema Export"
    ExporterName = ""
    PartnerName = ""    
    MediaFolder = "./media/"
    StyleFile = "./lib/default.css"
    ThumbWidth = 480
    
    # internal variables
    _assert_exit = 1
    Verbose = 0
    OldDate = ""
    PendingMsg = 0
    ONR = 0
    iOS = 0
    USDateFmt = 0
    Banner = "threema2html.awk - Convert a Threema chat export to nicely formatted, long term preservation friendly HTML"
    
    # process options
    while ((C = getopt(ARGC, ARGV, "f:t:T:n:p:m:s:w:iuvh")) != -1) 
    {
        if (C == "h") 
        {
            usage()
            exit
        }
        if (C == "i") 
            iOS = 1
        if (C == "u") 
            USDateFmt = 1            
        if (C == "v") 
            Verbose = 1
        if (C == "f") 
            DateFrom = Optarg * 1
        if (C == "t") 
            DateTo = Optarg * 1
        if (C == "T") 
            Title = Optarg 
        if (C == "n") 
            ExporterName = Optarg        
        if (C == "p") 
            PartnerName = Optarg        
        if (C == "m") 
            MediaFolder = Optarg
        if (C == "s") 
            StyleFile = Optarg
        if (C == "w") 
            ThumbWidth = Optarg * 1
    }
     
    # clear arguments, so that awk does not try to process the command-line options as file names 
    # (https://www.gnu.org/software/gawk/manual/html_node/Getopt-Function.html).
    for (I = 1; I <= Optind; I++)
    {
        if (substr(ARGV[I], 1, 1) == "-")
            ARGV[I] = ""
    }
    
    # check options
    if (DateFrom < 19700101 || DateFrom > 20701231)
    {
        print "Invalid -f date: " DateFrom
        usage()
        exit
    }
    
    if (DateTo < 19700101 || DateTo > 20701231)
    {
        print "Invalid -t date: " DateTo
        usage()
        exit
    }
    
    if (ThumbWidth < 16 || ThumbWidth > 3200)
    {
        print "Invalid -w width: " ThumbWidth
        usage()
        exit
    }

    _assert_exit = 0    # real work starts now
    
    if (Verbose)
    {
        print Banner                            > "/dev/stderr"
        print "Parameters in effect:"           > "/dev/stderr"
        print "\tDateFrom     = " DateFrom      > "/dev/stderr"
        print "\tDateTo       = " DateTo        > "/dev/stderr"
        print "\tExporterName = " ExporterName  > "/dev/stderr"
        print "\tPartnerName  = " PartnerName   > "/dev/stderr"        
        print "\tTitle        = " Title         > "/dev/stderr"
        print "\tMediaFolder  = " MediaFolder   > "/dev/stderr"
        print "\tStyleFile    = " StyleFile     > "/dev/stderr"
        print "\tThumbWidth   = " ThumbWidth    > "/dev/stderr"
        print "\tiOS          = " iOS           > "/dev/stderr"
        print "\tUSDateFmt    = " USDateFmt     > "/dev/stderr"
        print "\tVerbose      = " Verbose       > "/dev/stderr"
    }
    
    # print file header
    print "<!DOCTYPE html>"
    print "<html>"
    print "<head>"
    print "\t<meta name=\"generator\" content=\"threema2html.awk (https://codeberg.org/harveyk/threema2html/, kramski@web.de)\">"
    print "\t<title>" Title "</title>"
    print "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"" StyleFile "\">"
    print "</head>"
    print "<body>"
}

#------------------------------------------------------------------------------
/^<<<|^>>>/ && iOS {   # 'normal' input line starting with ">>>" or "<<<" (iOS)
#------------------------------------------------------------------------------
    if (substr($0, 5, 1) == "(") # we have a real user name, this should be a group chat
    # <<< (Heinz Werner Kramski-Grote) 8. August 2020 um 16:32:11 MESZ: Ah, ok
    {
        # process user
        split($0, A, /[()]/)
        User = A[2]
        
        # process timestamp
        split(A[3], B, / /)
        
        Day = B[2]
        Month = B[3]
        Year = B[4]
        # Time = substr(A[6], 1, 5)
        Time = B[6]
    }
    else # we have no real user name, this is either Me in a group chat or a 1:1 chat
    # >>> 10. April 2022 um 09:01:52 MESZ: Leider sehr einseitig auf Kunst bezogener Artikel :)        
    # <<< 27. März 2022 um 15:08:47 MESZ: OK, das IST krasser.
    {
        # process user & timestamp
        split($0, A, / /)
        User = A[1]
        Day = A[2]
        Month = A[3]
        Year = A[4]
        # Time = substr(A[6], 1, 5)
        Time = A[6]
    }
    
    # process message
    split($0, A, / (MEZ|MESZ): /) # fixme for other timezones
    Msg = A[2]  
    
    # process citations (not available on Android exports)
    if (match(Msg, /^\[.*"\] /, A))
    # <<< 11. April 2022 um 22:12:15 MESZ: [Ich: "Aber wir haben auch schon einen spanischen Wein getrunken"] Aber echt...        
    {
        split(A[0], B, " \"")
        CitUser = substr(B[1], 2, length(B[1]) - 2)
        CitMsg = substr(B[2], 1, length(B[2]) - 3)
        Msg = "<span class=\"citation\"><span class=\"cituser\">" UserDisplay(CitUser) "</span> <span class=\"citmsg\">" CitMsg "</span></span>" substr(Msg, length(A[0]))
    }
        
    # iOS to Android:
    $0 = "[" Day Name2Num(Month) Year ", " Time "] " User ": " Msg

    # print
    # next
}

#------------------------------------------------------------------------------
/^\[[0-9][0-9]/ {   # 'normal' input line starting with "[$timestamp]" (Android)
#------------------------------------------------------------------------------
    # [10/02/2018, 18:34] Me: Wir brauchen jetzt mal flugs die Adresse.
    
    # close previous message 
    if (PendingMsg)
    {
        CloseMsgDiv()
    }
    
    # process current line
    split($0, A, /^\[[0-9\.\/, :]+\] /)
    Msg = A[2]
    
    # process timestamp
    DateTime = substr($0, 2, length($0) - length(Msg) - 3)
    
    split(DateTime, A, /[, ]/)
    Date = A[1]
    Time = A[2]

    split(Date, A, /[\.\/]/)
    
    YYYY = A[3] * 1
    
    if (USDateFmt)
    {
        MM = A[1] * 1
        DD = A[2] * 1
    }
    else
    {
        MM = A[2] * 1
        DD = A[1] * 1
    }

    if (YYYY > 2070 || YYYY < 1970 || MM > 12 || MM < 1 || DD > 31 || DD < 1)
    {
        print "Invalid timestamp: " DateTime ", Record " FNR " ignored." > "/dev/stderr"
        PendingMsg = 0
        next
    }

    
    Date2 = sprintf("%04d%02d%02d", YYYY, MM, DD) * 1 # yyyymmdd 
    
    if (Date2 < DateFrom || Date2 > DateTo)
    {
        if (Verbose)
            print "Timestamp out of range: " DateTime ", Record " FNR " ignored." > "/dev/stderr"
        PendingMsg = 0
        next
    }

    if (Date != OldDate)
    {
        # close previous date div
        if (PendingMsg)
        {
            print "\t</div>"
        }
        
        # start new date group
        print "\t<div class=\"date\">"
        print "\t\t<h2>" Date "</h2>"
        OldDate = Date
    }
    
    # process user
    N = split(Msg, A, ":")
    if (N > 1)
    {
        User = A[1]
        Msg = substr(Msg, length(User) + 3)
    }
    else # there are a few malformed(?) lines starting with "[$timestamp]" but missing a user name
    {
        User = "Unknown"
    }

    # start new message
    print "\t\t<div class=\"" UserClass(User) "\">"
    print "\t\t\t<h3>" UserDisplay(User) "</h3>"

    Msg = MakeLinks(Msg)
    
    # print Msg
    print "\t\t\t<p class=\"msg\">"
    print "\t\t\t\t" Msg

    ONR++
    PendingMsg = 1
    
}

#------------------------------------------------------------------------------
!/^\[[0-9][0-9]/ && PendingMsg {   # continuation line
#------------------------------------------------------------------------------
    print "\t\t\t\t</br>" MakeLinks($0)

}

#------------------------------------------------------------------------------
END {
#------------------------------------------------------------------------------
    # don't output anything if we did not really start any work
    if (_assert_exit)
    {
        exit 
    }
    
    # close everything
    if (PendingMsg)
    {
        CloseMsgDiv()
    }
    
    print "\t</div>"    # date
    print "</body>"
    print "</html>"
        
    print NR " input records, " ONR " output messages." > "/dev/stderr"

}

#------------------------------------------------------------------------------
function MakeLinks(Text)
#------------------------------------------------------------------------------
{
    if (iOS)
    {
        # process images for inline display (current style)
        # >>> 29. November 2020 um 11:14:05 MEZ: Datei: 1dc452d8338f7f12-image-20202911-111404.jpeg
        Text = gensub(/Datei: (.+\.(jpe?g|png))/, "<br/><a href=\"" MediaFolder "\\1\"><img src=\"" MediaFolder "\\1\" alt=\"Image\" width=\"" ThumbWidth "\"/></a>", "g", Text)    
        
        # process images for inline display (old style?)
        # <<< 13. Juli 2020 um 23:21:22 MESZ: Bild (7132c769aa4676ca.jpg)
        Text = gensub(/Bild \((.+\.(jpe?g|png))\)/, "<br/><a href=\"" MediaFolder "\\1\"><img src=\"" MediaFolder "\\1\" alt=\"Image\" width=\"" ThumbWidth "\"/></a>", "g", Text)    
        
        # process audio files
        # >>> 19. April 2022 um 18:03:46 MESZ: Datei: 53206f90f2d6f746-recordAudio.m4a
        Text = gensub(/Datei: (.+\.(aac|m4a))/, "<audio controls src=\"" MediaFolder "\\1\">\\1</audio>", "g", Text)    

        # process other files as links
        # <<< 27. April 2022 um 14:33:44 MESZ: Datei: 2252d71cc48a7c11-MOUNTAINBIKE_2=0220228_3019fdb9eb7a4013a23053976da5c9ea.pdf
        Text = gensub(/Datei: (.+\.(mp4|pdf|vcf|doc|docx|zip))/, "<a href=\"" MediaFolder "\\1\">\\1</a>", "g", Text)    

        # process hyperlinks
        # <<< 1. Dezember 2022 um 07:47:30 MEZ: Steamdeck zu gewinnen jede Minute: https://help.steampowered.com/en/faqs/view/5DD7-287C-7548-299D 
        Text = gensub(/(https?:\/\/[^ \r]+)/, "<a href=\"\\1\">\\1</a>", "g", Text)
    }
    else
    {
        # process images for inline display
        # [24.10.2017, 23:04] Zweinz: Bild <f88d9071-c9f3-4196-8e9b-97a1775905f2.jpg>
        Text = gensub(/<(.+\.(jpe?g|png))>/, "\n\t\t\t\t<br/><a href=\"" MediaFolder "\\1\"><img src=\"" MediaFolder "\\1\" alt=\"Image\" width=\"" ThumbWidth "\"/></a>", "g", Text)    
        
        # process audio files
        # [21.4.2018, 09:38] Ich: Audio (00:03) <e248dd95-09fb-4406-bcd8-47089954ae13.aac>
        Text = gensub(/<(.+\.(aac|m4a))>/, "\n\t\t\t\t<audio controls src=\"" MediaFolder "\\1\">\\1</audio>", "g", Text)    

        # process other files as links
        # [9.3.2019, 13:28] Leonie: Datei: BoardingPass.pdf <fa7d22641ec75eb9-BoardingPass.pdf>
        Text = gensub(/<(.+\.(mp4|pdf|vcf|doc|docx|zip))>/, "\n\t\t\t\t<a href=\"" MediaFolder "\\1\">\\1</a>", "g", Text)    

        # process hyperlinks
        # [24.10.2017, 23:04] Zweinz: https://mars.nasa.gov/participate/send-your-name/insight/
        Text = gensub(/(https?:\/\/[^ ]+)/, "<a href=\"\\1\">\\1</a>", "g", Text) 
    }
    
    # sanitize XML characters
    Text = gensub(/&/, "&amp;", "g", Text)
    
    return (Text)
}

#------------------------------------------------------------------------------
function UserClass(User)
#------------------------------------------------------------------------------
{
    if (ExporterName && (User == "Ich" || User == "Me" || User == ">>>"))
        return (gensub(/ /, "_", "g", ExporterName) " Me") # multiple classes are allowed, separated by blanks
    else if (User == ">>>")
        return ("Me")
    else if (PartnerName && (User == "<<<"))
        return (gensub(/ /, "_", "g", PartnerName)) 
    else if (User == "<<<")
        return ("Them")
    else
        return (gensub(/ /, "_", "g", User))
}

#------------------------------------------------------------------------------
function UserDisplay(User)
#------------------------------------------------------------------------------
{
    if (ExporterName && (User == "Ich" || User == "Me" || User == ">>>"))
        return (ExporterName)
    else if (User == ">>>")
        return ("&gt;&gt;&gt;")
    else if (PartnerName && (User == "<<<"))
        return (PartnerName) 
    else if (User == "<<<")
        return ("&lt;&lt;&lt;")
    else
        return (User)
}

#------------------------------------------------------------------------------
function Name2Num(Month)
#------------------------------------------------------------------------------
{
    MNum["Januar"] = 1
    MNum["Februar"] = 2
    MNum["März"] = 3
    MNum["April"] = 4
    MNum["Mai"] = 5
    MNum["Juni"] = 6
    MNum["Juli"] = 7
    MNum["August"] = 8
    MNum["September"] = 9
    MNum["Oktober"] = 10
    MNum["November"] = 11
    MNum["Dezember"] = 12
    
    MNum["January"] = 1
    MNum["February"] = 2
    MNum["March"] = 3
    MNum["May"] = 5
    MNum["June"] = 6
    MNum["July"] = 7
    MNum["October"] = 10
    MNum["December"] = 12
    
    return (MNum[Month] ".")
}

#------------------------------------------------------------------------------
function CloseMsgDiv()
#------------------------------------------------------------------------------
{
    print "\t\t\t</p>"
    print "\t\t\t<p class=\"timestamp\">"     
    print "\t\t\t\t" DateTime
    print "\t\t\t</p>"
    print "\t\t</div>"
}
