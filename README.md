threema2html
============

Convert a Threema chat export to nicely formatted, long term preservation friendly HTML

(Was https://github.com/hkramski/threema2html, but moved to https://codeberg.org/harveyk/threema2html.)

1. For long-running exports, make sure that your smartphone does not go into standby mode during the 
   export process otherwise you may end up with a corrupted .zip file.
2. Export a chat in Threema (including media files), see https://threema.ch/en/faq/chatexport.
3. Unpack the .zip file (without any subfolders) into the folder where this .awk script lives.
4. Run the following commands in a terminal:
- `$ gawk -f threema2html.awk messages.txt > index.html`
   (See `gawk -f threema2html.awk -- -h` for more options. Use `-i` for experimental support for exports made on Apple iOS devices.)
- Copy relevant media files:
    ```
    $ grep "href=\"./media/" index.html | cut -d= -f2 | cut -d\" -f2 | cut -d/ -f3 > medialist.txt
    $ mkdir media
    $ xargs --arg-file=medialist.txt cp --target-directory=./media/
    ```
5. Adjust `./lib/default.css`.

This script has largely been tested on Android based exports only. 
Support for exports made on Apple iOS devices is experimental and currently limited to German.

Screenshot
----------

![Demo Screenshot](https://codeberg.org/harveyk/threema2html/raw/branch/master/demo.png "Demo Screenshot")
